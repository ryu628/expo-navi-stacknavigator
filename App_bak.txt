import React from 'react';
import { Button, View, Text, TouchableOpacity , StyleSheet, FlatList , AsyncStorage} from 'react-native';
import { createAppContainer, createStackNavigator } from 'react-navigation'; // Version can be specified in package.json

export default class App extends React.Component {
  render() {
    return <AppContainer />;
  }
}

const styles = StyleSheet.create({
  view:{
    flex: 1, 
    alignItems: 'center', 
    justifyContent: 'center',
    margin:10

  },
  button: {
      margin: 20,
      padding: 10,
      backgroundColor: 'wheat'
  }
});


class HomeScreen extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      signedIn: false,
      name: "",
      photoUrl: "",
      items:[],
      token:"",
      datas:[]
    }
  }


  // async componentDidMount(){

  //   const value



    // try {
    //   value = await AsyncStorage.getItem('access_token');
    //   if (value !== null) {
    //     console.log("We have data !!")
    //     console.log(value);
    //   }else{
    //     console.log("We have no data !!")
    //   }
    // } catch (error) {
     
    // }


    //  //access_tokenのチェック
    //  try{
    //   let url="https://www.googleapis.com/oauth2/v3/tokeninfo?access_token=" + "aa"
    //   let response = await fetch(url);
    //   let responseJson = await response.json();
    //   console.log("access_tokenのチェック成功1")
    //   console.log(responseJson)
    //   console.log(responseJson)
    //   console.log("access_tokenのチェック成功2")
    // } catch (error) {
    //   console.log("access_tokenのチェック失敗")
    // }



    // console.log("componentDidMount")
    // try {
    //   const result = await Expo.Google.logInAsync({
    //     androidClientId:
    //       "479255871486-fa02vpsijc9c1ne76r3tkdosmm9dri2h.apps.googleusercontent.com",
    //       scopes: ["profile", "email","https://www.googleapis.com/auth/spreadsheets"]
    //   })
    //   if (result.type === "success") {
    //     this.setState({
    //       signedIn: true,
    //       name: result.user.name,
    //       photoUrl: result.user.photoUrl,
    //       token:result.accessToken
    //     })
    //     console.log(result.accessToken)
    //   } else {
    //     console.log("cancelled")
    //   }
    // } catch (e) {
    //   console.log("error", e)
    // }


    // try {
    //   let aaa = "https://sheets.googleapis.com/v4/spreadsheets/1gN427Kx7OUfeys1wQ5ng5dVJjgAxfKbnjJ-KNwHi1qc/values:batchGet"
    //   + "?ranges=A1:B10000&majorDimension=DIMENSION_UNSPECIFIED&valueRenderOption=FORMATTED_VALUE&dateTimeRenderOption=FORMATTED_STRING"
    //   + "&access_token=" + "ya29.Glt6Blpj5vPQdFJn9HNkoyNXII1aDWJXqRieNZa1a1M5h2UtsrruYdPyWN7sHvYFsuPGSDC_cPDRwt9zlo5gMYN8CtJ3VgQp1uHLAE-KCNlLJLW7WD1KboZ-ITuL"
      // let aaa = "https://sheets.googleapis.com/v4/spreadsheets/1gN427Kx7OUfeys1wQ5ng5dVJjgAxfKbnjJ-KNwHi1qc/values:batchGet"
      // + "?ranges=A1:B10000&majorDimension=DIMENSION_UNSPECIFIED&valueRenderOption=FORMATTED_VALUE&dateTimeRenderOption=FORMATTED_STRING"
      // + "&access_token=" + this.state.token

      // console.log(aaa)
      // let response = await fetch(aaa);
      // let responseJson = await response.json();
      // console.log("API成功")
      // console.log(responseJson)
      // console.log(responseJson.valueRanges[0].values)

      // // 取得したSheet配列にkeyをつけてjsonへ変更
      // let spread  = responseJson.valueRanges[0].values
      // spread.map(function(value,index,array){
      //   array[index] = {key : index.toString() , english : value[0] , japanease : value[1]} 
      // })

      // //AsyncStorageにspreadを保存
      // try {
      //   await AsyncStorage.setItem('access_token', "ya29.Glt6Blpj5vPQdFJn9HNkoyNXII1aDWJXqRieNZa1a1M5h2UtsrruYdPyWN7sHvYFsuPGSDC_cPDRwt9zlo5gMYN8CtJ3VgQp1uHLAE-KCNlLJLW7WD1KboZ-ITuL");
      // } catch (error) {
      //   console.log("AsyncStorage保存エラー")
      //   console.error(error);
      // }

    //   //stateにspreadをセット
    //   this.setState({datas:spread})
    // } catch (error) {
    //   console.log("APIエラー")
    //   console.error(error);
    // }

    // console.log(this.state.datas)
    
  // }

  

  async handleClick(e){

    try {
      const value = await AsyncStorage.getItem('access_token');
      if (value !== null) {
        console.log("We have data !!")
        console.log(value);
      }else{
        console.log("We have no data !!")
      }
     } catch (error) {
       // Error retrieving data
     }

  }
  
  render() {


    return (
      <View style={styles.view}>

        {/* {this.state.datas.length == 0 ? <Text></Text> : 
          <FlatList
          data={this.state.datas}
          renderItem={({item}) => <Text>{item.english}:{item.japanease}</Text>}
          />   
        } */}
        <Text>Home Screen</Text>

        {/* <TouchableOpacity style={styles.button} onPress={(e) => this.handleClick(e)}>
          <Text>Click!</Text>
        </TouchableOpacity> */}

        <TouchableOpacity style={styles.button} onPress={() => this.props.navigation.navigate('Details')}>
          <Text>Go to Details</Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.button} onPress={() => this.props.navigation.navigate('SequenceQuest')}>
          <Text>Go to SeqQuest</Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.button} onPress={() => this.props.navigation.navigate('RandomQuest')}>
          <Text>Go to RandomQuest</Text>
        </TouchableOpacity>

      </View>
    );
  }
}

class DetailsScreen extends React.Component {

  render() {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>Details Screen</Text>
        <Button
          title="Go to Details... again"
          onPress={() => this.props.navigation.push('Details')}
        />
        <Button
          title="Go to Home"
          onPress={() => this.props.navigation.navigate('Home')}
        />
        <Button
          title="Go back"
          onPress={() => this.props.navigation.goBack()}
        />
      </View>
    );
  }
}

class SequenceQuestScreen extends React.Component {
  render() {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>SequenceQuest　Screen</Text>
        <Button
          title="Go back"
          onPress={() => this.props.navigation.goBack()}
        />
      </View>
    );
  }
}

class RandomQuestScreen extends React.Component {
  render() {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>RandomQuest　Screen</Text>
        <Button
          title="Go back"
          onPress={() => this.props.navigation.goBack()}
        />
      </View>
    );
  }
}


const RootStack = createStackNavigator(
  {
    Home: {
      screen: HomeScreen,
    },
    Details: {
      screen: DetailsScreen,
    },
    SequenceQuest: {
      screen: SequenceQuestScreen,
    },
    RandomQuest: {
      screen: RandomQuestScreen,
    },
  },
  {
    initialRouteName: 'Home',
  }
);

const AppContainer = createAppContainer(RootStack);

