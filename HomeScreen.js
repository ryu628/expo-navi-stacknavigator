import React from 'react';
import { Button, View, Text, TouchableOpacity , StyleSheet, FlatList , AsyncStorage} from 'react-native';
import { createAppContainer, createStackNavigator } from 'react-navigation'; // Version can be specified in package.json


export default class HomeScreen extends React.Component {

    constructor(props) {
      super(props)
      this.state = {
        signedIn: false,
        name: "",
        photoUrl: "",
        accessToken: "",
        refreshToken: "" ,
        expires_in : "" ,
        sheetData : ""
      }
    }

 
   /**
   * render
   *
   */
    render() {
  
  
      return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
          <Text>Home Screen</Text>
          <Button
            title="Go to Details..."
            onPress={() => this.props.navigation.push('Details')}
          />
        </View>
      );
    }
  }

  const styles = StyleSheet.create({
    view:{
      flex: 1, 
      alignItems: 'center', 
      justifyContent: 'center',
      margin:10,
    },
    button: {
        margin: 5,
        padding: 5,
        backgroundColor: 'rgba(255, 0, 0, 0.5)',
        borderRadius: 10,
    },
    text:{
      color: 'white'
    }
  });