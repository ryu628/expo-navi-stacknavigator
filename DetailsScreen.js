import React from 'react';
import { Button, View, Text, TouchableOpacity , StyleSheet, FlatList , AsyncStorage} from 'react-native';

const styles = StyleSheet.create({
    view:{
      flex: 1, 
      alignItems: 'center', 
      justifyContent: 'center',
      margin:10
  
    },
    button: {
        margin: 20,
        padding: 10,
        backgroundColor: 'wheat'
    }
  });

export default class DetailsScreen extends React.Component {

    render() {
      return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
          <Text>Details Screen</Text>
          <Button
            title="Go to Details... again"
            onPress={() => this.props.navigation.push('Details')}
          />
          <Button
            title="Go to Home"
            onPress={() => this.props.navigation.navigate('Home')}
          />
          <Button
            title="Go back"
            onPress={() => this.props.navigation.goBack()}
          />
        </View>
      );
    }
  }