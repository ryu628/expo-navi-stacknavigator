import React from 'react';
import { Button, View, Text, TouchableOpacity , StyleSheet, FlatList , AsyncStorage} from 'react-native';
import { createAppContainer, createStackNavigator } from 'react-navigation'; // Version can be specified in package.json
import HomeScreen from './HomeScreen'
import DetailsScreen from './DetailsScreen'

export default class App extends React.Component {
  render() {
    return <AppContainer />;
  }
}

const styles = StyleSheet.create({
  view:{
    flex: 1, 
    alignItems: 'center', 
    justifyContent: 'center',
    margin:10

  },
  button: {
      margin: 20,
      padding: 10,
      backgroundColor: 'wheat'
  }
});



// class DetailsScreen extends React.Component {
//   render() {
//     return (
//       <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
//         <Text>SequenceQuest　Screen</Text>
//         <Button
//           title="Go back"
//           onPress={() => this.props.navigation.goBack()}
//         />
//       </View>
//     );
//   }
// }


const RootStack = createStackNavigator(
  {
    Home: {
      screen: HomeScreen,
    },
    Details: {
      screen: DetailsScreen,
    },
  },
  {
    initialRouteName: 'Home',
  }
);

const AppContainer = createAppContainer(RootStack);

